import { memo } from 'react'
import { Link } from 'react-router-dom'
import styles from './style.module.scss'

const ListCard = memo(({ ability, stopLink = false }) => {
  const urlSplit = ability.url.trim().split('/')
  const abilityName = ability.name
  const abilityId = urlSplit[urlSplit.length - 2]

  return stopLink ? (
    <div className={styles.cardWrapper}>{abilityName}</div>
  ) : (
    <Link to={{ pathname: `/detail/${abilityId}/${abilityName}` }} className={styles.cardWrapper}>
      {abilityName}
    </Link>
  )
})

export default ListCard
