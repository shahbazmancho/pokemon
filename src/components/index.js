import { memo } from 'react'
import styles from './style.module.scss'

const Header = () => {
  return (
    <header className={styles.headerWrapper}>
      <img className={styles.headerIcon} src="/images/git-logo.png" alt="Git Hub" />
      <div className={styles.headerText}>
        <h1 className={styles.headerTextTitle}>GitHub Searcher</h1>
        <h2 className={styles.headerTextDesc}>Search users repositories below</h2>
      </div>
    </header>
  )
}

export default memo(Header)
