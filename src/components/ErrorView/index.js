import { memo } from 'react'
import styles from './style.module.scss'

const ErrorView = ({ code }) => {
  return (
    <div
      className={styles.errorContainer}
      style={{
        background: `url("https://wallpaperaccess.com/full/109967.jpg") no-repeat`,
        backgroundSize: 'cover',
      }}
    >
      <img src="/images/pokemon-logo.png" className={styles.errorImage} alt="Profile" />

      {code === 404 ? (
        <img src="/images/error_not_found.png" className={styles.errorNotFound} alt="Profile" />
      ) : (
        <h1>Whoops, something went wrong... </h1>
      )}
    </div>
  )
}

export default memo(ErrorView)
