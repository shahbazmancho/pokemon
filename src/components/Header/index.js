import { memo } from 'react'
import styles from './style.module.scss'

const Header = ({ name = null }) => {
  return (
    <header className={styles.headerWrapper}>
      <img className={styles.headerLogo} src="/images/pokeball-logo.png" alt="Pokemon" />
      <div className={styles.headerText}>
        <h1 className={styles.headerTextTitle}>Pokemon</h1>
        <h2 className={styles.headerTextDesc}>
          {name ? `Your favorite pokemon` : 'Select your favorite pokemon'}
        </h2>
      </div>
    </header>
  )
}

export default memo(Header)
