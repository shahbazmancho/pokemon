import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import loadable from '@loadable/component'
import './App.scss'

const HomePage = loadable(() => import('./pages/HomePage'))
const DetailPage = loadable(() => import('./pages/DetailPage'))
const NotFound = loadable(() => import('./pages/NotFound'))

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/detail/:id/:name?" component={DetailPage} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  )
}

export default App
