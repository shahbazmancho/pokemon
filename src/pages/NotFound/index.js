import React from 'react'
import ErrorView from '../../components/ErrorView'

const NotFound = () => {
  return <ErrorView code={404} />
}

export default NotFound
