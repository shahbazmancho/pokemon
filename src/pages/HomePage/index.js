import { useEffect, useMemo, useState } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import ErrorView from '../../components/ErrorView'
import Header from '../../components/Header'
import ListCard from '../../components/ListCard'
import Pokemon, { pokemonListEndpoint } from '../../models/Pokemon'

import styles from './style.module.scss'

const HomePage = () => {
  const [abilities, setAbilities] = useState([])
  const [error, setError] = useState(null)
  const [next, setNext] = useState(pokemonListEndpoint)

  const fetchData = () => {
    Pokemon.getPokemons(next, response => {
      if (response.error) {
        setError(response.message)
      } else {
        const { results, count, next, previous } = response
        setNext(next)
        setAbilities(abilities => abilities.concat(results))
      }
    })
  }

  const onFetchMore = () => {
    if (next) {
      setTimeout(() => {
        fetchData()
      }, 1000)
    }
  }
  useEffect(() => {
    !error && setError(null)
    fetchData()
  }, [])
  const count = useMemo(() => abilities.length, [abilities])
  const hasMore = useMemo(() => {
    return next != null
  }, [next])

  return (
    <div className="container">
      <Header />
      {error ? (
        <ErrorView />
      ) : (
        <InfiniteScroll
          dataLength={count} //This is important field to render the next data
          next={onFetchMore}
          hasMore={hasMore}
          loader={<h4>Loading...</h4>}
          className={styles.cardContainer}
        >
          {abilities.map((ability, index) => {
            return <ListCard ability={ability} key={ability.name + index} />
          })}
          )
        </InfiniteScroll>
      )}
    </div>
  )
}

export default HomePage
