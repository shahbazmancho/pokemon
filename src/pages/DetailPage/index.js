import { useEffect, useMemo, useState } from 'react'
import { withRouter } from 'react-router-dom'
import ErrorView from '../../components/ErrorView'
import Header from '../../components/Header'
import ListCard from '../../components/ListCard'
import Pokemon from '../../models/Pokemon'

import styles from './style.module.scss'

const DetailPage = ({ match }) => {
  const [pokemonData, setPokemonData] = useState(null)
  const [evolutionChain, setEvolutionChain] = useState(null)
  const [error, setError] = useState(null)

  const id = match.params.id
  useEffect(() => {
    Pokemon.getPokemon(id, response => {
      if (response.error) {
        setError(response.message)
      } else {
        setPokemonData(response)
      }
    })
    Pokemon.getEvolutionChain(id, response => {
      if (response.error) {
        setError(response.message)
      } else {
        setEvolutionChain(response)
      }
    })
  }, [match,id])

  const detail = useMemo(() => {
    return {
      name: pokemonData ? pokemonData.name : match.params.name,
      mainImage: pokemonData && pokemonData.sprites.other.dream_world.front_default,
      weight: pokemonData && pokemonData.weight,
      height: pokemonData && pokemonData.height,
      types: pokemonData ? pokemonData.types : [],
    }
  }, [match, pokemonData])

  const evolutions = useMemo(() => {
    let result = []
    if (evolutionChain) {
      const chain = evolutionChain.chain
      let evolvesTo = chain.evolves_to

      while (evolvesTo.length > 0) {
        const currentEvolution = evolvesTo[0]
        evolvesTo = currentEvolution.evolves_to
        result.push(currentEvolution.species)
      }
    }

    return result
  }, [evolutionChain])

  return(
    <div className="container">
      <Header name={detail.name} />

      {error ? (
        <ErrorView />
      ) : (
        <>
          <div className={styles.bannerWrapper}>
            <h1 className={styles.bannerTitle}>{detail.name}</h1>
            <img className={styles.bannerImage} src={detail.mainImage} />
            <div className={styles.bannerFooter}>
              <div className={styles.bannerFooterInfo}>
                <span>
                  {detail.weight} {detail.weight && 'kg'}
                </span>
                <span>Weight</span>
              </div>
              <div className={styles.bannerFooterTypes}>
                {detail.types.map(({ type }, index) => {
                  return (
                    <span key={index}>
                      {type.name} {index < detail.types.length - 1 && '/'}
                    </span>
                  )
                })}
              </div>
              <div className={styles.bannerFooterInfo}>
                <span>
                  {detail.height} {detail.height && 'm'}
                </span>
                <span>Height</span>
              </div>
            </div>
            <hr />
          </div>

          <div className={styles.evolutionWrapper}>
            <h2 className={styles.evolutionWrapper}>Evolution</h2>

            <div className={styles.evolutionList}>
              {evolutions.map(evolution => {
                return <ListCard ability={evolution} stopLink={true} key={evolution.name} />
              })}
            </div>
          </div>
        </>
      )}
    </div>
  )
}

export default withRouter(DetailPage)
