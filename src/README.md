<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://pngimg.com/uploads/pokemon/pokemon_PNG149.png" alt="Project logo"></a>
</p>

<h2 align="center"><a href="https://searcher-git.netlify.app/">Pokemon</a></h2>

---

<p align="center"> In this project you can explore pokemon and their evolution.
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Built Using](#built_using)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>

In this project we are using Pokemon APIs to fetch list of pokemon and their evolution.

We are using React js library.

## 🏁 Getting Started <a name = "getting_started"></a>

Download project or clone from gitlab

```
git clone https://gitlab.com/shahbazmancho/pokemon.git

```
### Installing

Once you have the project on your machine simply install the required modules by using yarn or npm

```
yarn install
```

OR

```
npm install
```

## 🔧 Running the tests <a name = "tests"></a>

You can run the test by running command

<!-- ### Break down into end to end tests

Explain what these tests test and why -->

```
yarn test
```
OR
```
npm test
```

## 🚀 Deployment <a name = "deployment"></a>

It is deeployed on <a href="https://netlify.com">Netlify</a>

## ⛏️ Built Using <a name = "built_using"></a>

- [ReactJs](http://reactjs.org//) - Library
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- [@shahbazmancho](https://gitlab.com/shahbazmancho) - Idea & Initial work






