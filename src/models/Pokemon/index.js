import axios from 'axios'

const REACT_APP_API_URL = process.env.REACT_APP_API_URL
const baseUrl = REACT_APP_API_URL ? REACT_APP_API_URL : 'https://pokeapi.co/api/v2'

export const pokemonListEndpoint = `${baseUrl}/pokemon?offset=0&limit=90`
const Pokemon = {
  getPokemons: (url, callback) => {
    return axios
      .get(url)
      .then(response => {
        const data = response.data

        return callback(data)
      })
      .catch(error => {
        console.log({ error })
        return callback({ error: true, message: error.message })
      })
  },
  getPokemon: (id, callback) => {
    return axios
      .get(`${baseUrl}/pokemon/${id}`)
      .then(response => {
        const data = response.data
        return callback(data)
      })
      .catch(error => {
        return callback({ error: true, data: error })
      })
  },
  getEvolutionChain: (id, callback) => {
    return axios
      .get(`${baseUrl}/evolution-chain/${id}`)
      .then(response => {
        const data = response.data
        return callback(data)
      })
      .catch(error => {
        return callback({ error: true, data: error })
      })
  },
}

export default Pokemon
